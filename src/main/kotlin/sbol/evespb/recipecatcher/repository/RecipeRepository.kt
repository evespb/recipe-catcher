package sbol.evespb.recipecatcher.repository

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository
import sbol.evespb.recipecatcher.domain.Recipe

interface RecipeRepository : ElasticsearchRepository<Recipe,String> {
}