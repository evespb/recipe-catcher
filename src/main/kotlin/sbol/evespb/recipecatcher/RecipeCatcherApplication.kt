package sbol.evespb.recipecatcher

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import sbol.evespb.recipecatcher.service.RecipeGrabberService

@SpringBootApplication
@EnableConfigurationProperties
class RecipeCatcherApplication()

fun main(args: Array<String>) {
    runApplication<RecipeCatcherApplication>(*args)
}
