package sbol.evespb.recipecatcher.service

import kotlinx.coroutines.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import sbol.evespb.recipecatcher.domain.Recipe
import sbol.evespb.recipecatcher.interfaces.CategoryGrabber
import sbol.evespb.recipecatcher.interfaces.GrabberService
import sbol.evespb.recipecatcher.interfaces.SourceGrabber
import sbol.evespb.recipecatcher.repository.RecipeRepository
import javax.annotation.PostConstruct

@Service
class RecipeGrabberService(
    val categoryGrabber: CategoryGrabber, val sourceGrabber: SourceGrabber, val recipeRepository: RecipeRepository
) : GrabberService {
    private val logger = LoggerFactory.getLogger(RecipeGrabberService::class.java)

    @PostConstruct
    override fun work() {

        try {
            recipeRepository.deleteAll()
            categoryGrabber.parseCategoryUrl()?.forEach {
                GlobalScope.launch {
                    val recipeList = sourceGrabber.parseSourceUrl(it)
                    logger.info("SAVE IN ELASTIC {}", recipeList.size)
                    recipeRepository.saveAll(recipeList)
                }
            }
        } catch (e: Exception) {
            logger.error("Error in work method with exception ", e)
        }
    }
}