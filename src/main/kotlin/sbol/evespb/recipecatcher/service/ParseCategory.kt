package sbol.evespb.recipecatcher.service

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import sbol.evespb.recipecatcher.interfaces.CategoryGrabber
import java.io.IOException

@Service
class ParseCategory : CategoryGrabber {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun parseCategoryUrl(): Collection<String?>? {
        val sourceList = arrayListOf<String>()
        try {
            val doc: Document = Jsoup.connect("https://eda.ru/recepty").userAgent("Chrome/4.0.249.0 Safari/532.5")
                .referrer("http://www.google.com").get()
            doc.select("li[class=seo-footer__list-item]").map { it.childNode(1).absUrl("href") }
                .toCollection(sourceList)
        } catch (e: IOException) {
            logger.error(e.message)
        }
        return sourceList
    }

}