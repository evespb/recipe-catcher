package sbol.evespb.recipecatcher.service

import org.jsoup.Jsoup
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import sbol.evespb.recipecatcher.domain.Recipe
import sbol.evespb.recipecatcher.interfaces.RecipeGrabber
import sbol.evespb.recipecatcher.interfaces.SourceGrabber
import sbol.evespb.recipecatcher.repository.RecipeRepository
import java.io.IOException
import java.util.concurrent.atomic.AtomicInteger


@Service
class ParseSource(val recipeGrabber: RecipeGrabber) : SourceGrabber {
    private val logger = LoggerFactory.getLogger(ParseSource::class.java)

    override fun parseSourceUrl(url: String?): List<Recipe?> {
        val linkList = arrayListOf<String>()
        val atomicInteger = AtomicInteger(1)
        val stringBuilder = StringBuilder(url).append("?page=")
        while (true) {
            try {
                val sourceUrl = stringBuilder.toString() + atomicInteger.getAndIncrement()
                val doc =
                    Jsoup.connect(sourceUrl).userAgent("Chrome/4.0.249.0 Safari/532.5")
                        .referrer("http://www.google.com")
                        .get()
                val links = doc.select("h3[class=horizontal-tile__item-title item-title]")
                if (links.size == 0) {
                    break
                }
                links.map { it.childNode(1).absUrl("href") }
                    .toCollection(linkList)
            } catch (e: IOException) {
                logger.error("Exception in method: parseSourceUrl() {}", e.message)
            }
        }
        return linkList.map {
            recipeGrabber.parseData(it)
        }

    }

}