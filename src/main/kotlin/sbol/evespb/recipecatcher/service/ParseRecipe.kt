package sbol.evespb.recipecatcher.service

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import sbol.evespb.recipecatcher.domain.Ingredients
import sbol.evespb.recipecatcher.domain.Recipe
import sbol.evespb.recipecatcher.interfaces.RecipeGrabber
import sbol.evespb.recipecatcher.repository.RecipeRepository
import java.io.IOException
import java.util.concurrent.atomic.AtomicReference

@Service
class ParseRecipe() : RecipeGrabber {
    private val logger = LoggerFactory.getLogger(ParseRecipe::class.java)


    override fun parseData(url: String?): Recipe? {
        return try {
            val doc =
                Jsoup.connect(url).userAgent("Chrome/4.0.249.0 Safari/532.5").referrer("http://www.google.com").get()
            val recipe = parseRecipeBody(doc)
            parseIngredientsList(recipe, doc)
            logger.info("CATCH {}",url)
            recipe
        } catch (e: IOException) {
            logger.error("Exception in method: parseData() ", e)
            return null
        }
    }


    override fun parseRecipeBody(document: Document?): Recipe? {
        val recipeblock = document?.select("section[class=recipe js-portions-count-parent js-recipe ]")
        val title = recipeblock?.select("h1[class=recipe__name g-h1]")?.text()
        val quantity = recipeblock?.select("span[class=info-text js-portions-count-print]")?.text()
        val time = recipeblock?.select("span[class=info-text]")?.text()
        val desc = recipeblock?.select("ul[class=recipe__steps]")?.text()
        return Recipe(
            title = title,
            time = time,
            description = desc,
            quantity = quantity,
            url = "",
            ingredientsList = ""
        )
    }

    override fun parseIngredientsList(recipe: Recipe?, document: Document?): Recipe? {
        try {
            val mapper = ObjectMapper()
            val r = document?.select("div[class=ingredients-list__content]")?.first()
            val obj = r?.select("p[class=ingredients-list__content-item content-item js-cart-ingredients]")
            val stringBuilder = StringBuffer("ИНГРЕДИЕНТЫ: ")
            if (obj != null) {
                for (e in obj) {
                    val v = e.select("p[class=ingredients-list__content-item content-item js-cart-ingredients]")
                        .attr("data-ingredient-object")
                    val ingredients: Ingredients = mapper.readValue(v, Ingredients::class.java)
                    stringBuilder.append(" ").append(ingredients.name.toLowerCase()).append(". ")
                }
            }
            if (recipe != null) {
                recipe.ingredientsList = (stringBuilder.toString())
            }
        } catch (e: JsonProcessingException) {
            logger.error("Exception in method: parseIngredientsList() ", e)
        }
        return recipe
    }

}

