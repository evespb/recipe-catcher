package sbol.evespb.recipecatcher.interfaces

import org.jsoup.nodes.Document
import sbol.evespb.recipecatcher.domain.Recipe

interface RecipeGrabber {
    fun parseData(url: String?): Recipe?
    fun parseRecipeBody(document: Document?): Recipe?
    fun parseIngredientsList(recipe: Recipe?, document: Document?): Recipe?
}
