package sbol.evespb.recipecatcher.interfaces


interface CategoryGrabber  {
    fun parseCategoryUrl(): Collection<String?>?
}
