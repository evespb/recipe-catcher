package sbol.evespb.recipecatcher.interfaces

import sbol.evespb.recipecatcher.domain.Recipe

interface SourceGrabber {
    fun parseSourceUrl(url: String?) :List<Recipe?>
}
