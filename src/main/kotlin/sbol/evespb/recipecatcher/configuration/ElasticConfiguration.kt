package sbol.evespb.recipecatcher.configuration

import org.elasticsearch.client.RestHighLevelClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.elasticsearch.client.ClientConfiguration
import org.springframework.data.elasticsearch.client.RestClients
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories
import sbol.evespb.recipecatcher.repository.RecipeRepository

@Configuration
@EnableElasticsearchRepositories(basePackageClasses = [RecipeRepository::class])
class ElasticConfiguration : AbstractElasticsearchConfiguration() {
    @Value("\${spring.elasticsearch.host}")
    lateinit var host: String
    @Value("\${spring.elasticsearch.port}")
    lateinit var port: String

    override fun elasticsearchClient(): RestHighLevelClient {
        val clientConfiguration = ClientConfiguration.builder()
            .connectedTo("$host:$port")
            .build()
        return RestClients.create(clientConfiguration).rest()
    }
}