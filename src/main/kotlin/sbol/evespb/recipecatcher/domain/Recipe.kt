package sbol.evespb.recipecatcher.domain

import org.springframework.data.elasticsearch.annotations.Document
import java.util.*

@Document(indexName = "recipe_index")
data class Recipe(
     val id: String = UUID.randomUUID().toString(),
     val title: String?,
     val quantity: String?,
     val time: String?,
     val description: String?,
     var ingredientsList: String?,
     var url: String?

    )