package sbol.evespb.recipecatcher.domain

import java.util.*


data class Ingredients(
    val id:String = UUID.randomUUID().toString(),
    val name: String="",
    val amount: String=""
)